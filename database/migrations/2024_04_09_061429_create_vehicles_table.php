<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->id(); // 'id' es un campo de tipo bigint (equivalente a 'long' en algunos otros sistemas)
            $table->string('brand'); // 'brand' es equivalente a 'marca' y se almacena como varchar
            $table->string('plate')->unique(); // 'plate' es el número de placa del vehículo, almacenado como varchar y marcado como único
            $table->char('licence_required', 1); // 'licence_required' indica si se requiere licencia, almacenado como char de longitud 1
            $table->timestamps(); // Crea campos 'created_at' y 'updated_at' automáticamente
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('vehicles');
    }
};
