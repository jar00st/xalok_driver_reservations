<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {

        Schema::create('drivers', function (Blueprint $table) {
            $table->id(); // 'id' es un campo de tipo bigint (equivalente a 'long' en algunos otros sistemas)
            $table->string('name'); // 'name' almacenado como varchar
            $table->string('surname'); // 'surname' almacenado como varchar
            $table->char('licence', 1); // 'licence' indica el tipo de licencia, almacenado como char de longitud 1
            $table->timestamps(); // Crea campos 'created_at' y 'updated_at' automáticamente
        });

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('drivers');
    }
};
