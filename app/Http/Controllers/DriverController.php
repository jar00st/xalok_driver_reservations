<?php

namespace App\Http\Controllers;

use App\Models\Driver;
use App\Models\Vehicle;
use Illuminate\Http\Request;

class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $drivers = Driver::all(); // Obtiene todos los driver. todo paginar.

        return @inertia('Driver/DriverIndex', [
            'drivers' => $drivers
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return @inertia('Driver/DriverCreate');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|min:3',
            'surname' => 'required|min:3',
            'licence' => 'required|min:1',
        ]);

        // Proceder a guardar el conductor en la base de datos con los datos validados
        $driver = Driver::create($validated);
        // Redirigir al usuario a la vista de detalles del conductor actualizado
        return redirect()->route('drivers.show', $driver->id)->with('success', 'Conductor actualizado con éxito.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $driver=Driver::find($id);
        return @inertia('Driver/DriverDetail',[
            'driver'=>$driver
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $driver=Driver::find($id);
        return @inertia('Driver/DriverEdit',[
            'driver'=>$driver
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        // Validar los datos del request
        $validated = $request->validate([
            'name' => 'required|min:3',
            'surname' => 'required|min:3',
            'licence' => 'required|min:1',
        ]);

        // Buscar el conductor por ID y actualizarlo
        $driver = Driver::findOrFail($id);
        $driver->update($validated);

        // Redirigir al usuario a la vista de detalles del conductor actualizado
        return redirect()->route('drivers.show', $driver->id)->with('success', 'Conductor actualizado con éxito.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        // Buscar el conductor por ID
        $driver = Driver::findOrFail($id);

        // Eliminar el conductor
        $driver->delete();

        // Redirigir al usuario a la lista de conductor con un mensaje de éxito
        return redirect()->route('vehicles.index')->with('success', 'Vehículo eliminado con éxito.');
    }

    public function availableDrivers(Request $request, $date, $vehicleId)
    {
        // Primero, obtén el vehículo para saber qué licencia es necesaria
        $vehicle = Vehicle::findOrFail($vehicleId);

        // Luego, busca conductores que:
        // 1. Tienen la licencia requerida por el vehículo
        // 2. No están asignados a un viaje en la fecha especificada
        $availableDrivers = Driver::where('licence', $vehicle->licence_required)
            ->whereDoesntHave('trips', function ($query) use ($date) {
                $query->whereDate('date', $date);
            })->get();

        // Retorna los conductores disponibles como respuesta JSON
        return response()->json($availableDrivers);
    }
}
