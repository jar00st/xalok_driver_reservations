<?php

namespace App\Http\Controllers;

use App\Models\Vehicle;
use Illuminate\Http\Request;
use Inertia\Inertia;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $vehicles = Vehicle::all(); // Obtiene todos los vehículos. todo paginar.

        return @inertia('Vehicle/VehicleIndex', [
            'vehicles' => $vehicles
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return @inertia('Vehicle/VehicleCreate');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'brand' => 'required|min:3',
            'plate' => 'required|min:3',
            'licence_required' => 'required|min:1',
        ]);

        // Proceder a guardar el vehículo en la base de datos con los datos validados
        $vehicle = Vehicle::create($validated);
        // Redirigir al usuario a la vista de detalles del vehículo actualizado
        return redirect()->route('vehicles.show', $vehicle->id)->with('success', 'Vehículo actualizado con éxito.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $vehicle=Vehicle::find($id);
        return @inertia('Vehicle/VehicleDetail',[
            'vehicle'=>$vehicle
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $vehicle=Vehicle::find($id);
        return @inertia('Vehicle/VehicleEdit',[
            'vehicle'=>$vehicle
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        // Validar los datos del request
        $validated = $request->validate([
            'brand' => 'required|min:3',
            'plate' => 'required|min:3|unique:vehicles,plate,' . $id, // Excluye el vehículo actual de la validación unique
            'licence_required' => 'required|size:1',
        ]);

        // Buscar el vehículo por ID y actualizarlo
        $vehicle = Vehicle::findOrFail($id);
        $vehicle->update($validated);

        // Redirigir al usuario a la vista de detalles del vehículo actualizado
        return redirect()->route('vehicles.show', $vehicle->id)->with('success', 'Vehículo actualizado con éxito.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        // Buscar el vehículo por ID
        $vehicle = Vehicle::findOrFail($id);

        // Eliminar el vehículo
        $vehicle->delete();

        // Redirigir al usuario a la lista de vehículos con un mensaje de éxito
        return redirect()->route('vehicles.index')->with('success', 'Vehículo eliminado con éxito.');
    }

    public function availableVehicles(Request $request, $date)
    {
        // Busca vehículos que no están reservados en la fecha especificada
        $availableVehicles = Vehicle::whereDoesntHave('trips', function ($query) use ($date) {
            $query->whereDate('date', $date);
        })->get();

        // Retorna los vehículos disponibles como respuesta JSON
        return response()->json($availableVehicles);
    }
}
