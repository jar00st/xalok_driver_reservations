<?php

namespace App\Http\Controllers;

use App\Models\Trip;
use Illuminate\Http\Request;

class TripController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // Usar el método with() para cargar las relaciones
        $trips = Trip::with(['vehicle', 'driver'])->get();


        return @inertia('Trip/TripIndex', ['trips' => $trips]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return @inertia('Trip/TripCreate');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // Validación de los datos recibidos
        $request->validate([
            'date' => 'required|date',
            'vehicle_id' => 'required|exists:vehicles,id',
            'driver_id' => 'required|exists:drivers,id',
        ]);

        // Creación de la nueva reserva de viaje
        $trip = Trip::create([
            'date' => $request->date,
            'vehicle_id' => $request->vehicle_id,
            'driver_id' => $request->driver_id,
        ]);

        // Redireccionar o responder según corresponda
        return redirect()->route('reservations.index')->with('success', 'Reserva creada con éxito.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        // Buscar el viaje por ID
        $trip = Trip::findOrFail($id);

        // Eliminar el viaje
        $trip->delete();

        // Redirigir al usuario a la lista de viajes con un mensaje de éxito
        return redirect()->route('reservations.index')->with('success', 'Vehículo eliminado con éxito.');
    }
}
