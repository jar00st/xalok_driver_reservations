<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    use HasFactory;
    // Especifica los campos que se pueden asignar masivamente
    protected $fillable = [
        'name',
        'surname',
        'licence',
    ];

    public function trips(){
        return $this->hasMany(Trip::class);
    }
}
