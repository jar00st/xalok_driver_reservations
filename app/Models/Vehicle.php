<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    use HasFactory;
    // Especifica los campos que se pueden asignar masivamente
    protected $fillable = [
        'brand',
        'plate',
        'licence_required',
    ];

    public function trips(){
        return $this->hasMany(Trip::class);
    }
}
