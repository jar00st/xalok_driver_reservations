// store.js
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        selectedDate: null,
        selectedVehicle: null,
        selectedDriver: null
    },
    mutations: {
        setSelectedDate(state, date) {
            state.selectedDate = date;
        },
        setSelectedVehicle(state, vehicle) {
            state.selectedVehicle = vehicle;
        },
        setSelectedDriver(state, driver) {
            state.selectedDriver = driver;
        },
        resetReservation(state) {
            state.selectedDate = null;
            state.selectedVehicle = null;
            state.selectedDriver = null;
        }
    },
    actions: {
        selectDate({ commit }, date) {
            commit('setSelectedDate', date);
        },
        selectVehicle({ commit }, vehicle) {
            commit('setSelectedVehicle', vehicle);
        },
        selectDriver({ commit }, driver) {
            commit('setSelectedDriver', driver);
        },
        reset({ commit }) {
            commit('resetReservation');
        }
    }
});
