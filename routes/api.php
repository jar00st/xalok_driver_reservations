<?php

use App\Http\Controllers\DriverController;
use App\Http\Controllers\VehicleController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');
Route::get('/vehicles/available/{date}', [VehicleController::class, 'availableVehicles']);
Route::get('/drivers/available/{date}/{vehicleId}', [DriverController::class, 'availableDrivers']);
