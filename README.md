# Xalok dirver managnment

Descripción breve del proyecto, su propósito y alcance.

## Tecnologías y Herramientas Utilizadas

Este proyecto se desarrolló utilizando una variedad de tecnologías y herramientas para cumplir con los requisitos y facilitar el desarrollo. Las decisiones tecnológicas se basaron en la experiencia previa, la eficiencia del desarrollo y las necesidades específicas del proyecto.

- **Laravel**: Se eligió Laravel como framework backend por su robustez, facilidad de uso y la amplia comunidad que lo respalda. Permitió una configuración y un desarrollo rápidos para cumplir con las restricciones de tiempo del proyecto.

- **Jetstream**: Se utilizó Laravel Jetstream para proporcionar la funcionalidad de autenticación y gestión de usuarios. Jetstream ofrece una implementación segura y bien diseñada que ahorra tiempo en el desarrollo de estas características comunes pero complejas.

- **Inertia.js**: Para crear una experiencia de usuario más fluida y dinámica, se integró Inertia.js, permitiendo la creación de una SPA (Single Page Application) utilizando Laravel como backend sin necesidad de construir una API separada.

- **Vue.js**: Se seleccionó Vue.js para el frontend debido a su integración con Inertia.js, su reactividad y su facilidad de uso, lo que permitió desarrollar interfaces de usuario interactivas de manera eficiente.

- **Tailwind CSS**: Para el estilizado de la aplicación, se utilizó Tailwind CSS por su enfoque de utilidades y su capacidad para crear diseños personalizados rápidamente sin salir del contexto del HTML.

## Consideraciones de Desarrollo

- **Tiempo**: Dado el marco de tiempo limitado para el desarrollo del proyecto, se tomaron algunas decisiones para optimizar la eficiencia del desarrollo:

    - Se prefirió la rapidez y simplicidad sobre la implementación de patrones de diseño más complejos como el patrón de repositorio.

    - Se eligieron tecnologías y herramientas con las que el equipo estaba familiarizado para minimizar la curva de aprendizaje y acelerar el proceso de desarrollo.

- **Funcionalidad**: Se priorizaron las características principales y necesarias del proyecto, dejando las mejoras y características adicionales para futuras iteraciones.

## Instrucciones de Instalación

Ejecuta los siguientes comandos

git clone git@gitlab.com:jar00st/xalok_driver_reservations.git
cd xalok_driver_reservations
docker run --rm \
-u "$(id -u):$(id -g)" \
-v "$(pwd):/var/www/html" \
-w /var/www/html \
laravelsail/php83-composer:latest \
composer install --ignore-platform-reqs
docker-compose exec laravel.test bash
php artisan migrate
npm run dev

## Uso

Ingresa a localhost
registrate en el sitio y luego podras hacer login
podras crear editar vehiculos, conductores y vijaes
## Contribuciones y Mejoras

Implementar patron de repositorio, pruebas unitarias y validaciones adicionales

---

